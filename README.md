# Hello-PaperJS

A small [React](https://reactjs.org/) page for playing with Vector-Graphics and [Paper.js](http://paperjs.org/about/).

```bash
npm install
npm start
```

![](./hello-paperjs_svg-skribblepad.gif)