import React from 'react';
import logo from './logo.svg';
import './App.css';
import SketchPad from './SketchPad/SketchPad';


const App: React.FC = () => {


  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <code>
          Hello

        <a
            className="App-link"
            href="http://paperjs.org/"
            target="_blank"
            rel="noopener noreferrer"
          > Paper.js</a>
        </code>
        <p>
          Draw on the Canvas to see it rendered as SVG
        </p>
      </header>
      <SketchPad></SketchPad>
    </div>
  );
}

export default App;
