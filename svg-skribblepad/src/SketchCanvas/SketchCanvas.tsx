import React, { Component } from 'react';
import './SketchCanvas.css';
import paper, { view, MouseEvent, project, Path, Color } from 'paper';
import InkDetectionService from '../InkDetection/InkDetectionService';
import { InkRecognizerResult } from '../InkDetection/InkRecognizerResult';



interface Props {
  canvasId: string;
  strokeSize: number;
  strokeColor: string | Color;
  smoothEdges?: boolean;
  simplifyPaths?: boolean;

  onSvgUpdated: (svg: string) => void,
  onImageRecogniced: (recognitions: InkRecognizerResult) => void,
  azureInkrecognizerUrl: string,
  azureInkrecognizerApiKey: string
}

interface State {
  svg?: SVGElement;
  pathSizeRaw: number;
  pathSimplifiedSize: number;
  svgRawSize: number;
  svgSimplifiedSize: number;
}


class SketchCanvas extends Component<Props, State> {
  private currentPath: Path | undefined;

  constructor(props: Props) {
    super(props);

    this.state = {
      pathSizeRaw: 0,
      pathSimplifiedSize: 0,
      svg: undefined,
      svgRawSize: 0,
      svgSimplifiedSize: 0
    }
  }


  componentDidMount() {
    console.group('componentDidMount');
    console.count(`Setting up paperjs on canvas with id '${this.props.canvasId}'.`);

    const canvas = document.getElementById(this.props.canvasId);
    if (!canvas)
      throw new Error(`Unable to find canvas with specified canvasId '${this.props.canvasId}'`);

    console.log('Setting up project and view.');
    paper.setup(this.props.canvasId);

    console.log('Mouse events.')
    view.onMouseDown = this.handleMouseDown.bind(this);
    view.onMouseDrag = this.handleMouseDrag.bind(this);
    view.onMouseUp = this.handleMouseUp.bind(this);

    console.groupEnd();
  }


  private handleMouseDown(event: MouseEvent) {
    console.group(`onMouseDown: ${event.point}`);

    this.currentPath = new paper.Path();
    this.currentPath.strokeColor = this.props.strokeColor;
    this.currentPath.strokeWidth = this.props.strokeSize;
    this.currentPath.fullySelected = true;
    this.currentPath.moveTo(event.point);

    console.log('Starting a new path at %o', event.point);
    console.groupEnd();
  }


  private handleMouseDrag(event: MouseEvent) {
    console.log(`onMouseDrag`);

    if (!!this.currentPath) {
      this.currentPath.add(event.point);
      this.setState({ pathSizeRaw: this.currentPath.segments.length });
    }
  }


  private handleMouseUp(event: MouseEvent) {
    console.group(`onMouseUp: ${event.point}`);

    const exportSvg = () => {
      const svg = project.exportSVG({ asString: true }).toString();
      console.log(`Exported SVG-String with length %i`, svg.length)
      return svg;
    };

    let svg = exportSvg();
    this.setState({ svgRawSize: svg.length });

    if (!!this.currentPath) {

      new InkDetectionService().recognizeInk(
        this.currentPath.segments.map(s => s.point),
        this.props.azureInkrecognizerUrl,
        this.props.azureInkrecognizerApiKey,
      ).then(this.props.onImageRecogniced);

      // Tolerance optional: default 2.5
      if (this.props.simplifyPaths) {
        console.log('Simplifying SVG-Paths.');
        this.currentPath.simplify(100);
      }

      if (this.props.smoothEdges) {
        console.log('Smoothing SVG-Edges.');
        this.currentPath.smooth();
      }

      this.setState({ pathSimplifiedSize: this.currentPath.segments.length });
    }

    svg = exportSvg();
    this.setState({ svgSimplifiedSize: svg.toString().length });
    this.props.onSvgUpdated(svg);

    console.groupEnd();
  }


  render() {
    return (<div id="flex-canvas-component">
      <div id="drawing-infos">
        <span>Segments Raw: {this.state.pathSizeRaw}, </span>
        <span>Segments Simplified: {this.state.pathSimplifiedSize}, </span>
        <span>SVG Raw: {this.state.svgRawSize}, </span>
        <span>SVG Simplified: {this.state.svgSimplifiedSize}</span>
      </div>
      <canvas id={this.props.canvasId}></canvas>
    </div>)
  }

}


export default SketchCanvas;