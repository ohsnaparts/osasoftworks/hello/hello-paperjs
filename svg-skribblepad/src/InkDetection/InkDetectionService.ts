import { Point } from "paper";
import { InkRecognizerResult } from "./InkRecognizerResult";

export default class InkDetectionService {

  public async recognizeInk(points: Point[], apiUrl: string, subscriptionKey: string): Promise<InkRecognizerResult> {
    const body = JSON.stringify({
      "version": 1,
      "language": "en-US",
      "unit": "mm",
      "strokes": [
        { "id": 183, "points": points.map(p => `${p.x},${p.y}`).join(','), "language": "en-US" }
      ]
    });

    return await fetch(apiUrl, {
      headers: [
        ["Ocp-Apim-Subscription-Key", subscriptionKey],
        ["content-type", "application/json"]
      ],
      body,
      method: "PUT"
    }).then(response => response.json())
      .then(response => {
        console.log(response);
        return <InkRecognizerResult> response;
      });
  };

}