import { Recognition } from "./Recognition";
import { RecognitionAlternative } from "./RecognitionAlternative";


export interface RecognitionUnit extends Recognition {
  class: string;
  alternates: RecognitionAlternative[];
}
