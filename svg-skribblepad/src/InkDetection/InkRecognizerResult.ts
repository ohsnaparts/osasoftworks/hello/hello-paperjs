import { RecognitionUnit } from "./RecognitionUnit";

export interface InkRecognizerResult {
  recognitionUnits: RecognitionUnit[];
}
