import React, { Component } from 'react';
import "./SketchPad.css";
import SketchCanvas from '../SketchCanvas/SketchCanvas';
import SvgViewer from '../SvgViewer/SvgViewer';
import { InkRecognizerResult } from '../InkDetection/InkRecognizerResult';
import { Recognition } from '../InkDetection/Recognition';


interface State {
  svg: string,
  recognitionResult: string
}

interface Props {
}

class SketchPad extends Component<Props, State> {

  constructor(props: Props) {
    super(props);

    this.state = {
      svg: '',
      recognitionResult: ''
    };
  }

  private onSvgUpdated(svg: string) {
    this.setState({ svg });
  }

  private onImageRecogniced(recognitionResult: InkRecognizerResult) {
    const leafUnits = recognitionResult.recognitionUnits.filter(u => u.class === 'leaf');
    const alternatives: Recognition[] = leafUnits.map(u => u.alternates)
      .flat(1)
      .map(a => ({ recognizedText: a.recognizedString, category: a.category }))
      || [];

    const recognitions: Recognition[] = [
      ...leafUnits,
      ...alternatives
    ].filter(r => !!r && !!r.recognizedText);

    console.log()
    this.setState({ recognitionResult: recognitions.map(r => r.recognizedText).join(',') });
  }

  render() {
    return (
      <div>
        <div className="flex-row">
          <div className="flex-item">
            <SketchCanvas
              canvasId="sketchCanvas"
              strokeColor="black"
              strokeSize={10}
              onSvgUpdated={this.onSvgUpdated.bind(this)}
              onImageRecogniced={this.onImageRecogniced.bind(this)}
              azureInkrecognizerUrl="https://api.cognitive.microsoft.com/inkrecognizer/v1.0-preview/recognize"
              azureInkrecognizerApiKey=""
              simplifyPaths={true}
              smoothEdges={false}>
            </SketchCanvas>
          </div>

          <div className="flex-item">
            <SvgViewer svg={this.state.svg}></SvgViewer>
          </div>
        </div>
        <div className="recognitionResult">
          {this.state.recognitionResult}
        </div>
      </div>
    );
  }

}


export default SketchPad;