import React, { Component } from 'react';
import './SvgViewer.css';


interface Props {
  svg: string
}

interface State {
}

class SvgViewer extends Component<Props, State> {
  render() {
    return <div id="svg-viewer-component">{this.props.svg}</div>;
  }

}


export default SvgViewer;